﻿using SheridanLibrary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheridanLibrary.Domain.Entities
{
   public class BookCart
    {
        private List<Book> _bookCart;

        public IEnumerable<Book> _bookList
        {
            get { return _bookCart; }
        }
        public BookCart()
        {
            _bookCart = new List<Book>();
            
        }
        public void AddBook(Book book)
        {
            _bookCart.Add(book);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheridanLibrary.Domain.Entities
{
    public enum BookGenre
    {
        Fantasy,
        SciFi,
        Horror,
        Mystery,
        Thriller
    }
   public class Book
    {
        public string Name { get; set; }
        
        public string Author { get; set; }
        public BookGenre Genre { get; set; }
        public decimal Rating { get; set; }
        public int ID { get; set; }
    }
}

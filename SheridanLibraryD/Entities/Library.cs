﻿using SheridanLibrary.Domain.Pesistences;
using SheridanLibraryD.Pesistences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheridanLibrary.Domain.Entities
{
    public class Library
    {
        private IBookRespository _respository;

        public IEnumerable<Book> _bookList { get; set; }

        public Library()
        {
            _respository = new MockBookRespository();
            this._bookList = _respository.BookStore;
        }

        public IEnumerable<Book> FilterBooks(Func<Book,bool> matchBook)
        {
            foreach(Book currentB in _bookList)
                if (matchBook(currentB))
                {
                    yield return currentB;
                }
        }

    }
}

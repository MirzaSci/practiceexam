﻿using SheridanLibrary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheridanLibrary.Domain.Pesistences
{
    interface IBookRespository
    {
        IEnumerable<Book> BookStore { get; }
    }
}

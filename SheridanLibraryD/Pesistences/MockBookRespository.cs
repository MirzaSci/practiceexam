﻿using SheridanLibrary.Domain.Pesistences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SheridanLibrary.Domain.Entities;

namespace SheridanLibraryD.Pesistences
{
    public class MockBookRespository : IBookRespository
    {
        public IEnumerable<Book> BookStore
        {
            get
            {
                return new List<Book>
                {
                    new Book {Name = "Slient Night", Author="Michael", Genre = BookGenre.Horror, Rating = 9.00m, ID =001 },
                    new Book {Name = "The Glasses Man", Author="Richard", Genre = BookGenre.Fantasy, Rating = 9.50m , ID =002},
                    new Book {Name = "Karate Man", Author="Ken", Genre = BookGenre.Mystery, Rating = 8.75m , ID =003},
                    new Book {Name = "The Boy Who Cried Wolf", Author="Jaimin", Genre = BookGenre.Fantasy, Rating = 7.59m , ID =004},
                    new Book {Name = "Detective Mirza", Author="Fahad", Genre = BookGenre.Thriller, Rating = 4.79m , ID =005},
                    new Book {Name = "How to Train Your Sphine", Author="Mena", Genre = BookGenre.Fantasy, Rating = 6.40m, ID =006 },
                    new Book {Name = "Star Trek: The wrath of Marco", Author="Marco", Genre = BookGenre.SciFi, Rating = 2.34m, ID =007 },
                    new Book {Name = "How to Get Prefect on the Exam", Author="Madgin", Genre = BookGenre.Fantasy, Rating = 10.00m, ID =008 },
                    new Book {Name = "How to Run A Country", Author="Donlad.T", Genre = BookGenre.Horror, Rating = 0.00m, ID =009 }
                };
            }

           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SheridanLibrary.Domain.Entities;

namespace SheridanLibrary.Controllers
{
    public class HomeController : Controller
    {
        private Library _library;

        public HomeController()
        {
            _library = new Library();
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FilterProducts(string GenreFilter)
        {
            
            if (String.IsNullOrEmpty(GenreFilter))
            {
                return View("Library",_library._bookList);
            }
            else
            {
                BookGenre userSelect =(BookGenre) Enum.Parse(typeof(BookGenre),GenreFilter);
                
                return View("Library", _library.FilterBooks(b => b.Author == GenreFilter ));
            }
        }
        public ActionResult BookFilter(string title)
        {
            if (String.IsNullOrEmpty(title))
            {
                return View("Library",_library._bookList);
            }
            else
            {
                return View("Library", _library.FilterBooks(b => b.Name.IndexOf(title,StringComparison.OrdinalIgnoreCase)>=0));
            }
        }

        public ActionResult FilterRating(string rating)
        {
            decimal lowR = Decimal.Parse(rating);
            var rateList = from b in _library._bookList where b.Rating >= lowR select b;
            return View("Library", rateList);
        }
        public ActionResult Library()
        {
            return View("Library",_library._bookList);
        }
    }
}
﻿using SheridanLibrary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SheridanLibrary.Controllers
{
   
    public class CartController : Controller
    {
        private const string BOOK_SESSION = "BookCart";
        private Library _library;

        public CartController()
        {
            _library = new Library();
        }
        public BookCart _bookCart
        {
            get
            {
                BookCart cart = Session[BOOK_SESSION] as BookCart;
                if(cart == null)
                {
                    cart = new BookCart();
                    Session[BOOK_SESSION] = cart;
                }
                return cart;
            }
        }

        
        // GET: Cart
        public ActionResult Index(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View(this._bookCart);
        }

        public ActionResult AddToCart(int bookId, string returnUrl)
        {
            Book _book = _library._bookList.FirstOrDefault(b => b.ID == bookId);

            if (_book != null)
            {
                this._bookCart.AddBook(_book);
            }
            return RedirectToAction("Index", new { returnUrl });

        }

        public ActionResult cancelSession()
        {
            Session.Abandon();
            return RedirectToAction("Index","Home");
        }
    }    
}